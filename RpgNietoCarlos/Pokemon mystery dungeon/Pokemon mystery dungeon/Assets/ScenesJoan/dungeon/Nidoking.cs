﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Nidoking : MonoBehaviour
{
    public int vel;
    public float MinDist;
    private int move;
    private float DistX, DistY;
    private bool trigger;
    private static bool triggerfight;
    public Animator animator;
    Random Random = new Random();
    private GameObject Player;
    public Nidoking Enemy;
    // Start is called before the first frame update
    void Start()
    {
         Player = GameObject.Find("Player");
        animator = gameObject.GetComponent<Animator>();
        InvokeRepeating("Move", 2, 1);
    }

    // Update is called once per frame
    void Update()
    {
        Enemy.transform.position = new Vector3(Enemy.transform.position.x,
        Enemy.transform.position.y,
        Enemy.transform.position.z);

        DistX = Enemy.transform.position.x - Player.transform.position.x;
        DistY = Enemy.transform.position.y - Player.transform.position.y;

        if (Vector3.Distance(Enemy.transform.position, Player.transform.position) <= 2)
        {
            triggerfight = true;
            SceneManager.LoadScene("NidokingFight");
        }
        
      
    }

    void Hunt()
    {
        if (DistX <= MinDist)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", true);
            animator.SetBool("MoveB", false);
        }
        else if (DistX >= MinDist)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveB", true);
            animator.SetBool("MoveR", false);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", false);
            animator.SetBool("MoveB", false);
        }
       
    }
    void Move()
    {
        trigger = GameObject.Find("Player").GetComponent<Player>().getTriggerN();

        if (trigger)
        {
            Hunt();

        }
        else
        {

            move = Random.Range(1, 3);

            if (move == 1)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("MoveR", true);
                animator.SetBool("MoveB", false);
            }
            else if (move == 2)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("MoveB", true);
                animator.SetBool("MoveR", false);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("MoveR", false);
                animator.SetBool("MoveB", false);
            }
           
           
        }
    }
    public static bool getFight()
    {
        return triggerfight;
    }
    public static void setFight()
    {
        triggerfight = false;
    }
}
