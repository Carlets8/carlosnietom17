﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
     public GameObject player;
     public GameObject camara;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        camara = GameObject.Find("Main Camera");
    }

    // Update is called once per frame
    void Update()
    {
         camara.transform.position = new Vector3(
           player.transform.position.x,
           player.transform.position.y,
           camara.transform.position.z);

    }
}
