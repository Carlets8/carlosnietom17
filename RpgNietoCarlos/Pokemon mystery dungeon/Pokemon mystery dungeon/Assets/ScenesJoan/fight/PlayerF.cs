﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerF : MonoBehaviour
{
    private GameObject Player;
    private float hp, ataque, defensa, level;
    private int arapp = 35, burbpp = 25, hidropp = 5, surfpp = 15;
    private string atac1 = "Arañazo", atac2 = "Burbuja", atac3 = "Hidro Bomba";

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //Coger los estados de player, para usarlo en la lucha
        setHP(Player.GetComponent<Player>().getHp()); ;
       
        setAtaque(Player.GetComponent<Player>().getAtaque());

       setDefensa(Player.GetComponent<Player>().getDefensa());

       setLv(Player.GetComponent<Player>().getLv());
               
    }
    public float gethp()
    {
        return hp;
    }
    public float getLv()
    {
        return level;
    }
    public float getatac()
    {
        return ataque;
    }
    public float getdef()
    {
        return defensa;
    }
    public string getatac1()
    {
        return atac1;
    }
    public string getatac2()
    {
        return atac2;
    }
    public string getatac3()
    {
        return atac3;
    }

    public int gethidro()
    {
        return hidropp;
    }
    public int getarañazo()
    {
        return arapp;
    }
    public int getsurf()
    {
        return surfpp;
    }
    public int getburbuja()
    {
        return burbpp;
    }
    public void setHP(float hp)
    {
        this.hp = hp;
    }
    public void setAtaque(float ataque)
    {
        this.ataque = ataque;
    }
    public void setDefensa(float defensa)
    {
        this.defensa = defensa;
    }
    public void setLv(float level)
    {
        this.level = level;
    }
}
