﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    
    public GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
        //La camara coge la posicion inicial del player, al entrar al juego.
        this.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, -1);
    }

    // Update is called once per frame
    void Update()
    {
        //Si el player esta en los limites del mapa, la camara dejara de seguirle
        if (Player.transform.position.x > -34 && Player.transform.position.x<7.863699)
        {
            this.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, -1);
        }
            
                
    }
}
