﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownController : MonoBehaviour
{
    public AudioSource xxx;
    public AudioClip cancion;
    
    
    // Start is called before the first frame update
    void Start()
    {
        //Al entrar en la ciudad se reproducira la cancion y despues, cada 110 segundos que es lo que dura
        InvokeRepeating("playCancion",0,110);
    }

    // Update is called once per frame
    void Update()
    {
              
    }
    //Funcion para reproducir la cancion
    void playCancion(){
        xxx.PlayOneShot(cancion);
    }
}
