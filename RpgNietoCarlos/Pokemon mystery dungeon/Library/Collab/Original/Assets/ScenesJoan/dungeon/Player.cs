﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    //Dinero del jugador
    public int dinero=0;

    //Para activar las conversaciones;
    public Canvas ventanaDialogo;
    public Text textoDialogo;
    //Declaro la roca de la escena HotDungeon.
    public GameObject roca;
    private bool trigger;
    private bool triggerG;
    private bool triggerEB;
    private bool triggerMF;

    public int vel;
    public Animator animator;


    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        ventanaDialogo.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("g"))
        {
            animator.SetBool("Cabeza", true);
        }else{
            animator.SetBool("Cabeza", false);
        }
        
        if (Input.GetKey("d")) { 
          this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", true);
            animator.SetBool("MoveB", false);
        }
        else if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveB", true);
            animator.SetBool("MoveR", false);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", false);
            animator.SetBool("MoveB", false);
        }
        if (Input.GetKey("w"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel );
            animator.SetBool("MoveU", true);
            animator.SetBool("MoveD", false);
        }
        else if (Input.GetKey("s"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
            animator.SetBool("MoveD", true);
            animator.SetBool("MoveU", false);

        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
            animator.SetBool("MoveD", false);
            animator.SetBool("MoveU", false);

        }

        //Dialogo con Ampharos
        if (Dialogos.hablandoAmpharos)
        {
            if (Input.GetKeyDown("f"))
            {
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
                textoDialogo.text = "Ampharos: Bienvenidos a la banca etica! Aqui no invertimos en Pokeguerras." +
                    " Quires depositar o retirar dinero?";/*mejorar*/
                
            }            



        }
        else if(Dialogos.hablandoOnyx)
        {
            if(Input.GetKeyDown("f"))
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
            textoDialogo.text = "Onyxillo: Que pasa mariconas? Recordad no tener relaciones con Pokemons del mismo genero";
        }

        else
        {
            ventanaDialogo.GetComponent<Canvas>().enabled = false;
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "t1En")
        {
            trigger = true;
        }
        if (collision.gameObject.name == "bajar")
        {
            triggerEB = true;
        }
        if (collision.gameObject.name == "bajar2" || collision.gameObject.name == "bajarF")
        {
            triggerMF = true;
        }
        if (collision.gameObject.tag == "t2En")
        {
            triggerG = true;
        }
        if (collision.gameObject.tag == "Doramion")
        {
                if (Input.GetKey("g"))
                {
                    GameObject.Destroy(roca);
                }
        }
  
    }





    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "t1Ex")
        {
            trigger = false;
        }
          if (collision.gameObject.tag == "suelo")
        {
            triggerEB = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if(collision.gameObject.tag == "paredfalse")
        {
            Destroy(collision.gameObject);
        }
     

    }
    public bool getTrigger()
    {
        return trigger;
    }

    public bool getTriggerEB(){
         return triggerEB;
    }
    public bool getTriggerMF()
    {
        return triggerMF;
    }
    public bool getTriggerG()
    {
        return triggerG;
    }
}
