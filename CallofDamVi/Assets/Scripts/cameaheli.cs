﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameaheli : MonoBehaviour
{
    #region GameObjects
    [Header("GameObject")]
    public GameObject heli;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.forward = heli.transform.forward;
    }
}
