﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class navDroid : MonoBehaviour
{
    #region GameObjects
    [Header("gameObjects")]
    public Droid droid;
    private GameObject player;
    #endregion

    #region NavMesh
    [Header("Nav Agent")]
    public NavMeshAgent agent;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("RigidBodyFPSController");
    }

    // Update is called once per frame
    void Update()
    {
        if (0 < droid.hp)
        {
            this.transform.LookAt(player.transform);
            //Seteas el objetivo al que quieres que baya
            agent.SetDestination(player.transform.position);
        }
        else
        {
            this.agent.enabled = false;
        }
    }
}
