﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class disparo : MonoBehaviour
{
    #region Audios
    [Header("Audio")]
    public AudioClip l96;
    public AudioClip Ak47;
    public AudioClip reloadAudio;
    //public AudioClip Pistola;
    #endregion

    #region Cameras
    [Header("Camaras")]
    public Camera cam;
    #endregion

    #region Canvas
    [Header("Canvas")]
    public Canvas canvas;
    #endregion

    #region GameObjects
    [Header("GameObjects")]
    public GameObject impacto;
    public GameObject fuego;
    #endregion

    #region Text
    [Header("Text")]
    public Text txtBalas;
    #endregion

    #region Variables 
    //Total
    [Header("Variables")]
    public int balasTotalL96;
    public int balasTotalAK;
    public int balasTotalPistola;
    // private int audio;
    private int daño = 500;
    private int rango;
    private float cad;
    private bool disparado;
    private bool start;
    //Municion
    private int balasL96;
    private int balasAK;
    private int balasPistola;
    //Cargador
    // int cargL96;
    private int cargAK;
    private int cargPistola;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        #region initVariables
        rango = 0;
        cad = 0;
        disparado = false;
        start = true;
        //Municion
        balasL96 = 10;
        balasAK = 30;
        balasPistola = 6;
        //Cargador
        //cargL96 = 10;
        cargAK = 30;
        cargPistola = 6;
        //Total
        balasTotalL96 = 10;
        balasTotalAK = 150;
        balasTotalPistola = 17;
        #endregion
        //TODO Hacerlo scriptable object culpa andreu
    }

    // Update is called once per frame
    void Update()
    {
        cambioArma();

        //Disparar. Si hay balas disponibles
        disparar();
    }

    #region Metodos
    public void cambioArma()
    {
        if (Input.GetKeyDown("1")) //L96
        {
            transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);

        }
        else if (Input.GetKeyDown("2")) //Pistola
        {
            transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
            transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);

        }
        else if (Input.GetKeyDown("3")) //AK
        {
            transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(true);
            transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);

        }
        //Cambiar los valores de daño,rango.. segun el arma
        if (transform.GetChild(0).transform.GetChild(0).gameObject.active == true) //L96
        {
            daño = 500;
            rango = 400;
            cad = 1.5f;
            this.transform.GetChild(2).GetComponent<AudioSource>().clip = l96;
            txtBalas.text = balasL96.ToString() + " / " + balasTotalL96.ToString();
        }
        else if (transform.transform.GetChild(0).GetChild(1).gameObject.active == true) //Pistola
        {
            daño = 50;
            rango = 25;
            cad = 0.4f;
            txtBalas.text = balasPistola.ToString() + " / " + balasTotalPistola.ToString();
        }
        else if (transform.transform.GetChild(0).GetChild(2).gameObject.active == true) //AK
        {
            daño = 150;
            rango = 60;
            cad = 0.05f;
            this.transform.GetChild(2).GetComponent<AudioSource>().clip = Ak47;
            txtBalas.text = balasAK.ToString() + " / " + balasTotalAK.ToString();
        }
    }
    public void disparar()
    {
        if (Input.GetMouseButton(0))
        {
            if (transform.GetChild(0).transform.GetChild(0).gameObject.active == true && balasL96 > 0) //L96
            {
                // 1r con tempo 0, sino retrasa el disparo, una vez se esta disparando, ya tiene el tiempo correcto
                if (start)
                {
                    StartCoroutine(cadencia(0));
                    start = false;
                }
                else
                {
                    StartCoroutine(cadencia(cad));
                }
            }
            else if (transform.transform.GetChild(0).GetChild(1).gameObject.active == true && balasPistola > 0) //Pistola
            {
                if (start)
                {
                    StartCoroutine(cadencia(0));
                    start = false;
                }
                else
                {
                    StartCoroutine(cadencia(cad));
                }
            }
            else if (transform.transform.GetChild(0).GetChild(2).gameObject.active == true && balasAK > 0) //AK
            {
                if (start)
                {
                    transform.transform.GetChild(0).GetChild(2).transform.GetChild(0).gameObject.SetActive(true);
                    StartCoroutine(cadencia(0));
                    start = false;
                }
                else
                {
                    transform.transform.GetChild(0).GetChild(2).transform.GetChild(0).gameObject.SetActive(true);
                    StartCoroutine(cadencia(cad));

                }
            }
        }
        else if (Input.GetKeyDown("r"))
        {
            StartCoroutine(reload());
        }
        else if (Input.GetMouseButtonUp(0))
        {
            start = true;
            StopAllCoroutines();
        }
    }
    public void restoreAmmo()
    {
        //Restaura el cargador entero de la L96
        balasL96 = 10;

        //Da 2 cargadores a la AK y a la pistola
        if (balasTotalAK + (cargAK * 2) > 150)
        {
            balasTotalAK = 150;
        }
        else
        {
            balasTotalAK += cargAK * 2;
        }

        if (balasTotalPistola + (cargPistola * 2) > 17)
        {
            balasTotalPistola = 17;
        }
        else
        {
            balasTotalPistola += cargPistola * 2;
        }
    }
    public void daños(barril barrilete, Droid droide, RaycastHit hit)
    {

        if (barrilete != null)
        {
            barrilete.gethit = true;
        }
        /*  if (Heli != null)
          {
              Heli.hp -= daño;
          }*/

        if (droide != null)
        {
            if (hit.transform.tag == "head")
            {
                //si ha sido headshot
                droide.headshoot = true;
            }
            else if (hit.transform.tag != "head")
            {
                //Baja la vida del droide
                droide.hp -= daño;
            }
        }
    }
    public void gastarBalas()
    {
        //Restar municion
        if (transform.GetChild(0).transform.GetChild(0).gameObject.active == true) //L96
        {
            if (balasL96 > 0)
            {
                balasL96 -= 1;
            }

        }
        else if (transform.transform.GetChild(0).GetChild(1).gameObject.active == true) //Pistola
        {
            if (balasPistola > 0)
            {
                balasPistola -= 1;

            }
        }
        else if (transform.transform.GetChild(0).GetChild(2).gameObject.active == true) //AK
        {
            if (balasAK > 0)
            {
                balasAK -= 1;
            }

        }
    }
    #endregion

    #region Coorutinas
    IEnumerator reload()
    {

        yield return new WaitForSeconds(0.5f);
        if (transform.GetChild(0).transform.GetChild(0).gameObject.active == true) //L96
        {
            //El cargador de la L96 es unico           
        }
        else if (transform.transform.GetChild(0).GetChild(1).gameObject.active == true) //Pistola
        {

            if (balasTotalPistola >= (cargPistola - balasPistola))
            {
                balasTotalPistola -= cargPistola - balasPistola;
                balasPistola = cargPistola;
            }
            else
            {
                balasPistola += balasTotalPistola;
                balasTotalPistola = 0;
            }

        }
        else if (transform.transform.GetChild(0).GetChild(2).gameObject.active == true) //AK
        {

            if (balasTotalAK >= (cargAK - balasAK))
            {
                balasTotalAK -= cargAK - balasAK;
                balasAK = cargAK;
            }
            else
            {
                balasAK += balasTotalAK;
                balasTotalAK = 0;
            }
        }
    }

    IEnumerator cadencia(float time)
    {
        yield return new WaitForSeconds(time);
        RaycastHit hit;


        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, rango))
        {
            //Instancia el disparo
            if (!disparado)
            {
                //Coge los componentes del los objetivos si han sido alcanzados
                barril barrilete = hit.transform.GetComponent<barril>();
                Droid droide = hit.transform.GetComponent<Droid>();
               // heli Heli = hit.transform.GetComponent<heli>();
               //Crea el impacto
                GameObject shoot = Instantiate(impacto, hit.point, Quaternion.LookRotation(hit.normal));
                shoot.transform.LookAt(hit.point);
                shoot.GetComponent<impactoScript>().audios = this.transform.GetChild(2).GetComponent<AudioSource>().clip;

                gastarBalas();
                daños(barrilete, droide, hit);
                disparado = true;
            }

            //Stop la coroutina
            disparado = false;
            transform.transform.GetChild(0).GetChild(2).transform.GetChild(0).gameObject.SetActive(false);

            StopAllCoroutines();
        }
    }
    #endregion
}
