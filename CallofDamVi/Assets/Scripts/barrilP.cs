﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barrilP : MonoBehaviour
{
    #region Variables
    [Header("Variables")]
    public bool activado;
    private float radi = 2.0f;
    private float power = 200;
    private float upforce = 1;
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        activado = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (activado)
        {
            explosion();
            radioDaño();
        }
    }

    #region Metodos
    public void explosion()
    {
        //Activar hijos
        this.GetComponent<SphereCollider>().enabled = true; ;
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.transform.GetChild(2).gameObject.SetActive(true);
        this.transform.parent.transform.GetChild(1).gameObject.SetActive(true);
    }
    public void radioDaño()
    {
        //Coge su  poicion
        Vector3 explosion = this.transform.position;
        //esfera que crea para la explosion
        Collider[] colliders = Physics.OverlapSphere(explosion, radi);
        //foreach a todos los objectos que esten dentro del rango y aplica el daño i la fuerzas
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null && hit.tag == "droide")
            {
                rb.AddExplosionForce(power, explosion, radi, upforce, ForceMode.Impulse);
            }
            if (rb != null && hit.tag == "player")
            {
                rb.AddExplosionForce(70, explosion, radi, upforce, ForceMode.Impulse);
            }
        }
        StartCoroutine(killeffect());
    }
    #endregion

    #region Coorutinas
    IEnumerator killeffect()
    {
        yield return new WaitForSeconds(10);
        Destroy(this.gameObject);
    }
    #endregion


}
