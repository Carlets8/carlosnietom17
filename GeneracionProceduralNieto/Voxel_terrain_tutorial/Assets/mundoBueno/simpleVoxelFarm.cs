﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simpleVoxelFarm : MonoBehaviour {

    int cols;
    int rows;

	private GameObject currentBlockType;
	public GameObject[] blockTypes;
    public GameObject water;
    public GameObject tree;

    [Tooltip("True for minecraft style voxels")]
	public bool SnapToGrid = true;

    public int seed = 200;
	public float amp = 10f;
	public float freq = 10f;

	private Vector3 myPos;

    //Variables para el desplazamiento    

    public GameObject player;
    Vector3 terrainPos = Vector3.zero;
    Vector3 playerPos = Vector3.zero;
    Vector3 difference = Vector3.zero;



    void Start () {
        //el mapa de perlin siempre devuelve la misma posicion.
        //si tu quieres que devuelva algo distinto tendras que hacer un desfase. Ese defase es conocido como "seed" (semilla)
        //print(Mathf.PerlinNoise(5f, 2f));
		generateTerrain ();

        //Ponemos al jugador en medio del mapa
        player.transform.position = new Vector3(cols/2, 30, rows/2);

        //Calculamos la diferencia del terreno
        difference = player.transform.position - this.transform.position;
        difference.y = 0;

        difference.x = Mathf.Floor(difference.x);
        difference.z = Mathf.Floor(difference.z);

        playerPos.x = Mathf.Floor(player.transform.position.x);
        playerPos.z = Mathf.Floor(player.transform.position.z);
        playerPos.y = 0;

        terrainPos = playerPos - difference;
        this.transform.position = terrainPos;

	}

    private void Update()
    {
        //Actualizamos la posicion del jugador
        playerPos.x = Mathf.Floor(player.transform.position.x);
        playerPos.z = Mathf.Floor(player.transform.position.z);
        playerPos.y = 0;

        //Actualizamos la posicion del terreno
        terrainPos = playerPos - difference;
        this.transform.position = terrainPos;

        
        //generateTerrain();

    }



    void generateTerrain(){

        //determinas posicion inicial
		myPos = this.transform.position;

        //dices numero de filas y columnas
		cols = 70;
		rows = 70;

        //recorro el mapa como si fuera una matriz.
		for (int x = 0; x < cols; x++) {

			for (int z = 0; z < rows; z++) {
			
                //si la x,z son muy pequeñas al dividirlas por una freq grande se van a quedar a 0 y luego se multiplicaran por una amplitud grande.
                //perlinnoise va a devolver la intensidad del mapa perlin de la posicion
				float y = Mathf.PerlinNoise ((myPos.x + x + seed + this.transform.position.x) / freq, 
					          (myPos.z + z + this.transform.position.z) / freq) * amp;
                
			
				if (SnapToGrid) {
                    //fuerzas a que la y sea un entero (las diferencias seran a nivel bloque, como si fuera minecraft)
                    y = Mathf.Floor(y);
                }

                

                

                //Para los biomas
                if (z<rows/2 && x<cols/2)
                {
                    if (y > 3 * (amp / 4)) //Bloques de nieve
                        currentBlockType =
                        blockTypes[2];
                    else if (y > 2 * (amp / 5))//Bloques de tierra
                        currentBlockType = blockTypes[1];
                    else if (y > 1 * (amp / 5))//Bloques de hierba
                        currentBlockType =
                        blockTypes[0];
                    else //Bloques de piedra
                        currentBlockType =
                        blockTypes[3];
                } else
                {
                    currentBlockType =
                        blockTypes[2];
                }



                //si no hubiese el >=0, en los puntos que se queda justo a 0 habria un agujero
                for (float i = y; i >= y-5; i--)
                {

                    GameObject newBlock = GameObject.Instantiate(currentBlockType);
                    newBlock.transform.position =
                    new Vector3(myPos.x + x, i, myPos.z + z);

                    //int ran = Random.Range(0,100); 

                    /*
                     GameObject newTree =
                     GameObject.Instantiate(tree);
                     newTree.transform.position =
                     new Vector3(myPos.x + x, i+1, myPos.z + z);
                     */
                }

                for (float i = y; i < (amp/5); i++)
                {
                    GameObject waterBlock =
                  GameObject.Instantiate(water);

                    waterBlock.transform.position =
                        new Vector3(myPos.x + x, i, myPos.z + z);
                }



            }

		}

	
	}


}
