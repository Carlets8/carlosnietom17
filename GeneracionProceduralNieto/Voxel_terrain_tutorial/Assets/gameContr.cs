﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameContr : MonoBehaviour
{
    
    int cols = 5;
    int rows = 5;
    public GameObject cubo;
    public GameObject jugador;
    private Vector3 myPos;

    // Start is called before the first frame update
    void Start()
    {
        generarTerreno();
        jugador.transform.position = new Vector3(cols / 2, 2, rows / 2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void generarTerreno()
    {


        for (float x = 0; x <=cols; x++)
        {
            for (float y = 0; y <= rows; y++)
            {
                GameObject newBlock = GameObject.Instantiate(cubo);
                newBlock.transform.position =
                new Vector3(myPos.x + x, 1, myPos.z+y);
            }
        }

        


    }
}
