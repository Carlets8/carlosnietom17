﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barril : MonoBehaviour
{
    #region Variables
    [Header("Variable")]
    public bool gethit;
    #endregion

    #region GameObjects
    [Header("GameObject")]
    public barrilP padre;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        gethit = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (gethit)
        {
            padre.activado = true;
        }
    }

  
}
