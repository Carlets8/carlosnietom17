﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ammo : MonoBehaviour
{
    #region Variables
    private int muniSni;
    private int muniAK;
    private int muniPi;
    #endregion

    //Evento para hacer una recarga de municion general. Si tiene todos los cargadores al maximo, no saltará el evento.
    #region Eventos
    [Header("GameEvent")]
    public GameEvent restoreAmmo;
    [Space(20)]
    #endregion

    #region GameObjects
    GameObject jugador;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        muniSni = 10;
        muniAK = 150;
        muniPi = 15;
    }

    // Update is called once per frame
    void Update()
    {
    }


    private void OnCollisionEnter(Collision collision)
    {
        jugador = collision.gameObject;
        if (collision.transform.tag == "player")
        {

            //Si la municion está al maximo, no desperdiciaras la bolsa de munición
            if (jugador.GetComponent<disparo>().balasTotalAK < 150 || jugador.GetComponent<disparo>().balasTotalL96 < 10 ||
                jugador.GetComponent<disparo>().balasTotalPistola < 17)
            {
                //Llama al evento
                restoreAmmo.Raise();
                Destroy(this.gameObject);
            }

        }
    }
}
