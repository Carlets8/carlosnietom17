﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class characterSelectController : MonoBehaviour
{
    //Canvas
    public Canvas canvas;
    
    //Ints para controlar la seleccion
    int posPais = 0;
    int posCharP1 = 0;
    int posCharP2 = 1;

    //String para guardar el pais seleccionado
    string pais="";

    //String para guardar los players seleccionados
    string sp1 = "";
    string sp2 = "";

    //Bool para controlar que ya se haya seleccionado el pais
    bool bpais = false;

    //Bool para controlar que ya se hayan seleccionado los 2 personajes
    bool bjugador1 = false;
    bool bjugador2 = false;

    // Start is called before the first frame update
    void Start()
    {
        
        //Posicion inicial de los paises
        canvas.transform.GetChild(0).GetChild(3).gameObject.SetActive(true);
        canvas.transform.GetChild(0).GetChild(5).gameObject.SetActive(false);

        //Posicion inicial de la imagen grande de seleccion de personajes
        canvas.transform.GetChild(0).GetChild(8).gameObject.SetActive(false);
        canvas.transform.GetChild(0).GetChild(9).gameObject.SetActive(false);
        canvas.transform.GetChild(0).GetChild(10).gameObject.SetActive(false);
               

    }

    // Update is called once per frame
    void Update()
    {       

        if (!bpais)
        {

            if (Input.GetKeyDown(KeyCode.RightArrow) && posPais == 0)
            {
                canvas.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);
                canvas.transform.GetChild(0).GetChild(5).gameObject.SetActive(true);
                posPais++;
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow) && posPais == 1)
            {
                canvas.transform.GetChild(0).GetChild(3).gameObject.SetActive(true);
                canvas.transform.GetChild(0).GetChild(5).gameObject.SetActive(false);
                posPais--;
            }

            if (posPais == 0 && Input.GetKeyDown("return"))
            {
                pais = "japon";                
                bpais = true;

            }
            else if (posPais == 1 && Input.GetKeyDown("return"))
            {
                pais = "usa";                
                bpais = true;
            }

        } else
        {
            //Seleccion Jugador 1
            if (!bjugador1)
            {
                if (Input.GetKeyDown("d"))
                {
                    if (posCharP1 != 1)
                    {
                        canvas.transform.GetChild(0).GetChild(6).position = new Vector3(canvas.transform.GetChild(0).GetChild(6).position.x + 1, canvas.transform.GetChild(0).GetChild(6).position.y, canvas.transform.GetChild(0).GetChild(6).position.z);
                        posCharP1++;
                    }
                    else
                    {
                        canvas.transform.GetChild(0).GetChild(6).position = new Vector3(canvas.transform.GetChild(0).GetChild(6).position.x - 1, canvas.transform.GetChild(0).GetChild(6).position.y, canvas.transform.GetChild(0).GetChild(6).position.z);
                        posCharP1 = 0;
                    }

                }
                else if (Input.GetKeyDown("a"))
                {
                    if (posCharP1 != 0)
                    {
                        canvas.transform.GetChild(0).GetChild(6).position = new Vector3(canvas.transform.GetChild(0).GetChild(6).position.x - 1, canvas.transform.GetChild(0).GetChild(6).position.y, canvas.transform.GetChild(0).GetChild(6).position.z);
                        posCharP1--;
                    }
                    else
                    {
                        canvas.transform.GetChild(0).GetChild(6).position = new Vector3(canvas.transform.GetChild(0).GetChild(6).position.x + 1, canvas.transform.GetChild(0).GetChild(6).position.y, canvas.transform.GetChild(0).GetChild(6).position.z);
                        posCharP1 = 1;
                    }
                }
                if (Input.GetKeyDown("return"))
                {
                    bjugador1 = true;
                }
            }

            //Seleccion Jugador 2
            if (!bjugador2)
            {
                if (Input.GetKeyDown("p"))
                {
                    bjugador2 = true;
                }
            }

            //Imagen grande segun la posicion del indicador de seleccion de personaje 1
            if (posCharP1 == 0)
            {
                canvas.transform.GetChild(0).GetChild(8).gameObject.SetActive(true);
                canvas.transform.GetChild(0).GetChild(9).gameObject.SetActive(false);
                sp1 = "ryuHeavy";
            }
            else if (posCharP1 == 1)
            {
                canvas.transform.GetChild(0).GetChild(8).gameObject.SetActive(false);
                canvas.transform.GetChild(0).GetChild(9).gameObject.SetActive(true);
                sp1 = "ryu";
            }


            //Imagen grande segun la posicion del indicador de seleccion de personaje 2
            if (posCharP2 == 1)
            {
                canvas.transform.GetChild(0).GetChild(10).gameObject.SetActive(true);                
                sp2 = "ryu";
            }
            
        }

        //Cuando todo esta seleccionado
        if (bjugador1 && bjugador2)
        {
            PlayerPrefs.SetString("player1",sp1);
            PlayerPrefs.SetString("player2", sp2);
            PlayerPrefs.SetString("escenario", pais);
            SceneManager.LoadScene("pelea");
        }
        


    }

}
