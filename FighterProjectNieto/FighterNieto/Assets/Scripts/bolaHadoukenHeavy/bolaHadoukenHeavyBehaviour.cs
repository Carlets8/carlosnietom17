﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bolaHadoukenHeavyBehaviour : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {


        if (collision.tag == "player2" || collision.tag == "bolaHadouken")
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {

        if (this.tag=="bolaHeavy1")
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(6f, 8f, 0);
        } else if (this.tag == "bolaHeavy2")
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(20f, 2f, 0);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
