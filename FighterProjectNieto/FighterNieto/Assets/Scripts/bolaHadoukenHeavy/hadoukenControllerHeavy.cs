﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hadoukenControllerHeavy : MonoBehaviour
{
    //Determinar tipo de hadouken
    public GameObject hadouken;
    public GameObject hadouken2;

    //Prefabs
    public GameObject hadoukenPrefab;
    public GameObject hadoukenPrefab2;

    //Player
    public GameObject player;
    public bool hadoukenInstanciado=false;
    public bool hadoukenInstanciado2 = false;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        if (hadouken.activeSelf == true && !hadoukenInstanciado)
        {
            
            
            GameObject a = Instantiate(hadoukenPrefab) as GameObject;            
            a.transform.position = new Vector3(player.transform.position.x + 4, player.transform.position.y, player.transform.position.z);
            hadoukenInstanciado = true;
        }
        else if (hadouken.activeSelf == false && hadoukenInstanciado)
        {
            hadoukenInstanciado = false;
        }



        if (hadouken2.activeSelf == true && !hadoukenInstanciado2)
        {
            
            GameObject a = Instantiate(hadoukenPrefab2) as GameObject;
            a.transform.position = new Vector3(player.transform.position.x + 4, player.transform.position.y, player.transform.position.z);
            hadoukenInstanciado2 = true;

        } else if(hadouken2.activeSelf == false && hadoukenInstanciado2)
        {
            hadoukenInstanciado2 = false;
        }


    }
}
