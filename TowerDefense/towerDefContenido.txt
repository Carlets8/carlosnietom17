Descripción

Nuestro proyecto ha concluido en un tower defense al estilo Age of empires.
El juego consiste en construir edificios alrededor del nucleo, el cual es asediado por hordas de enemigos,
para defenderlo. paralelamente, puedes y debes mandar tropas a matar fauna salvaje, minar menas y talar madera
para obtener recursos y seguir defendiendote de las oleadas. Las oleadas se generan por tiempo y por fases,
cada nueva fase, disminuye el tiempo, aumentando la dificultad.

Toda la información de los recursos está disponible en el canvas. En la esquina superior izquierda.

Los jugadores jugables se controlan haciendo doble click en la zona del mapa donde quieres que se diriga un pj
y un click al personaje origen. Estos se generan con recursos, igual que las estructuras, y tienen un maximo de
cantidad.

Los personajes no jugables, en este caso el oso salvaje, detecta cuando hay un pj cerca y va a por el para eliminarlo.

El mapa esta compuesto por tres zonas y basado en un mapa isometrico, al igual que los personajes y los elementos
decorativos que lo componen.

Contenido extra

Inicialmente se pensó en mover a los personajes jugables con drag and drop. Actualmente lo hemos programado con clicks.
Igualmente esta opción está disponible descomentando el codigo.

Ya que habia que elegir entre tropas o torres, consideramos que al hacer las dos, una de ellas es contenido extra. Hemos
desarrollado tanto tropas que consiguen recursos, como torres que defienden la base.

Por ultimo, hemos retocado el script AI Destination para hacer el movimiento a*path del aldeano.

Participación

Mi participación en este proyecto ha sido:

Mapa de nieve, con sus colliders y propiedades.
Mitad de las animaciones del aldeano, knight y oso. Busqueda y preparacion de sus sprites.
Comportamiento del knight y del oso salvaje. (Scripts de comportamiento, combate por turnos etc)
a* de las oleadas de osos. (Pulida por Joan)
Comportamiento de las oleadas. Funciones para invocar y controlarlas.