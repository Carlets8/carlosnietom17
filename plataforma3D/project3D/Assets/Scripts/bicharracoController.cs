﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bicharracoController : MonoBehaviour
{

    public AudioSource audio;
    public Animator anim;
    public AudioClip conversacion1;
    public GameObject pared;
    public GameObject musicaAmbiente;
    bool intro = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player" && intro)
        {
            audio.Stop();            
            anim.SetBool("bailar", true);
            DestroyObject(pared.gameObject);
            audio.PlayOneShot(conversacion1);
            intro = false;            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {            
            musicaAmbiente.GetComponent<AudioSource>().Play();
            
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
