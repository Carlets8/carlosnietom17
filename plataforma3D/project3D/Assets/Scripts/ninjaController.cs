﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ninjaController : MonoBehaviour
{
    //Velocidad de movimiento
    float velMove = 1f;
    float velRotate = 100f;
    float mouseX, mouseY;

    //Velocidad de salto
    float jumpForce = 8f;

    //Animator
    private Animator animator;

    //Variables de movimiento
    public float x, y;

    //Control para saltar
    bool salto;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag=="floor")
        {
            salto = true;            
        }
    }

  

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        
        transform.Rotate(0,x*Time.deltaTime * velRotate,0);
        transform.Translate(0,0,y*Time.deltaTime*velMove);

        Debug.Log(y * Time.deltaTime * velMove);

        //Correr
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (salto)
            {
                velMove = 8f;
            }
            
            
        } else
        {
            velMove = 1f;
        } 

        //Andar mas lento hacia atras
        if (y<0)
        {
            velMove = 0.5f;
            animator.SetBool("walkB", true);
        } else
        {
            animator.SetBool("walkB", false);
        }

        //Saltar
        if (Input.GetKey("space") && salto && y * Time.deltaTime * velMove > 0.1f) {            
            this.GetComponent<Rigidbody>().AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);            
            salto = false;
        } else if ((Input.GetKey("space") && salto && y * Time.deltaTime * velMove < 0.1f))
        {
            this.GetComponent<Rigidbody>().AddForce(new Vector3(0, 6f, 0), ForceMode.Impulse);
            salto = false;
        }




        //Animaciones
        if (y * Time.deltaTime * velMove>0.01)
        {
            if (salto)
            {
                animator.SetBool("walk", true);
            }
            
        } else
        {
            animator.SetBool("walk", false);
        }
        
        if (y * Time.deltaTime * velMove>0.1)
        {
            if (salto)
            {
                animator.SetBool("run", true);
            }
             
        } else
        {
            animator.SetBool("run", false);
        } 



    }
}
